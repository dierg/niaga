<?php
// --------------------------------------------------------------------------
namespace App\Providers;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
class AppServiceProvider extends ServiceProvider {
    // ----------------------------------------------------------------------
    public function register(){
        if( env( 'REDIRECT_HTTPS' )){
            $this->app['request']->server->set( 'HTTPS', true );
        }
    }
    // ----------------------------------------------------------------------


    // ----------------------------------------------------------------------
    public function boot( UrlGenerator $url ){
        // ------------------------------------------------------------------

        // ------------------------------------------------------------------
        // Checking env to redirect over https
        // https://rtmccormick.com/2017/07/21/force-assets-to-use-https-laravel/
        // ------------------------------------------------------------------
        if( env( 'REDIRECT_HTTPS' )){
            $url->formatScheme( 'https' );
        }
        // ------------------------------------------------------------------
    }
    // ----------------------------------------------------------------------
}
// --------------------------------------------------------------------------
