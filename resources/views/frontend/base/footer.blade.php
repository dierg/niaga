    <!-- Footer - Start -->
    <footer class="footer">
        <div class="container">

            <!-- Footer links - Start -->
            <div class="row text-center text-sm-left">
                
                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Hubungi Kami</div>
                    <p class="text">
                        0274-5305505<br/>
                        Senin - Minggu<br/>
                        24 Jam Nonstop
                    </p>
                    <p class="text">
                        Jl. Selokan Mataram Monjali<br/>
                        Karangjati MT I/304<br/>
                        Sinduadi, Mlati, Sleman<br/>
                        Yogyakarta 55284
                    </p>
                </div>

                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Layanan</div>
                    <div class="list-group list-sitemap">
                        <a href="#" class="list-group-item">Domain</a>
                        <a href="#" class="list-group-item">Shared Hosting</a>
                        <a href="#" class="list-group-item">Cloud VPS Hosting</a>
                        <a href="#" class="list-group-item">Managed VPS Hosting</a>
                        <a href="#" class="list-group-item">Web Builder</a>
                        <a href="#" class="list-group-item">Keamanan SSL / HTTPS</a>
                        <a href="#" class="list-group-item">Jasa Pembuatan Website</a>
                        <a href="#" class="list-group-item">Program Afiliasi</a>
                    </div>
                </div>

                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Service Hosting</div>
                    <div class="list-group list-sitemap">
                        <a href="#" class="list-group-item">Hosting Murah</a>
                        <a href="#" class="list-group-item">Hosting Indonesia</a>
                        <a href="#" class="list-group-item">Hosting Singapura SG</a>
                        <a href="#" class="list-group-item">Hosting PHP</a>
                        <a href="#" class="list-group-item">Hosting Wordpress</a>
                        <a href="#" class="list-group-item">Hosting Laravel</a>
                    </div>
                </div>

                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Tutorial</div>
                    <div class="list-group list-sitemap">
                        <a href="#" class="list-group-item">Knowledgebase</a>
                        <a href="#" class="list-group-item">Blog</a>
                        <a href="#" class="list-group-item">Cara Pembayaran</a>
                    </div>
                </div>

                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Tentang Kami</div>
                    <div class="list-group list-sitemap">
                        <a href="#" class="list-group-item">Tim Niagahoster</a>
                        <a href="#" class="list-group-item">Karir</a>
                        <a href="#" class="list-group-item">Events</a>
                        <a href="#" class="list-group-item">Penawaran & Promo Sepcial</a>
                        <a href="#" class="list-group-item">Kontak Kami</a>
                    </div>
                </div>

                <div class="col-sm-4 col-lg-3 pt-5">
                    <div class="title uc mb-4">Kenapa Pilih Niagahoster?</div>
                    <div class="list-group list-sitemap">
                        <a href="#" class="list-group-item">Support Terbaik</a>
                        <a href="#" class="list-group-item">Garansi Harga Termurah</a>
                        <a href="#" class="list-group-item">Domain Gratis Selamanya</a>
                        <a href="#" class="list-group-item">Datacenter Hosting Terbaik</a>
                        <a href="#" class="list-group-item">Review Pelanggan</a>
                    </div>
                </div>

                <div class="col-sm-8 col-lg-3 pt-5">
                    <div class="title uc mb-4">Newsletter</div>
                    <div class="form-group subscribe rounded-pill">
                        <div class="row mx-n1">
                            <div class="px-1 col">
                                <input type="text" class="form-control rounded-pill" name="subscribe-email" placeholder="Email">
                            </div>
                            <div class="px-1 col-auto d-flex align-items-center">
                                <button type="button" class="btn btn-lg btn-primary rounded-pill px-2 fs-13">
                                    <span>Berlangganan</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <span class="form-text text-muted">Dapatkan promo dan konten menarik dari penyedia hosting terbaik Anda.</span>
                </div>

                <div class="col-sm-4 col-lg-3 pt-3 pt-sm-5">
                    <div class="title uc mb-4 d-none d-sm-block">&nbsp;</div>
                    <div class="row mx-n1 justify-content-center justify-content-md-between">

                        <div class="px-1 col-auto">
                            <a href="#" class="btn btn-social">
                                <div class="icon-container">
                                    <i class="fab fa-facebook-f"></i>
                                </div>
                            </a>
                        </div>

                        <div class="px-1 col-auto">
                            <a href="#" class="btn btn-social">
                                <div class="icon-container">
                                    <i class="fab fa-twitter"></i>
                                </div>
                            </a>
                        </div>

                        <div class="px-1 col-auto">
                            <a href="#" class="btn btn-social">
                                <div class="icon-container">
                                    <i class="fab fa-google-plus-g"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Footer links - End -->

            <!-- Payment - Start -->
            <div class="payment mt-5 text-center text-sm-left">
                <div class="title uc mb-2">Pembayaran</div>
                <div class="row mx-n2 justify-content-center justify-content-sm-start">
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                    <div class="px-2 col-auto">
                        <div class="gateway my-2">Bank</div>
                    </div>
                </div>
                <span class="form-text text-muted fs-14">Aktivasi instan dengan e-Payment. Hosting dan domain langsung aktif!</span>
            </div>
            <!-- Payment - End -->

            <div class="separator mt-4"></div>

            <!-- Copyright - Start -->
            <div class="copyright mt-3 pb-5">
                <div class="row mx-n2 text-center text-md-left">
                    <div class="px-2 col-md">
                        <p class="mb-1">Copyright &copy; 2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta</p>
                        <p class="mb-1">Cloud VPS murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                    <div class="px-2 col-md-auto mt-3 mt-md-0">
                        <div class="row mx-n1 justify-content-center">
                            <div class="px-1 col-auto">
                                <a href="#">Syarat dan Ketentuan</a>
                            </div>
                            <div class="px-1 col-auto">|</div>
                            <div class="px-1 col-auto">
                                <a href="#">Kebijakan Privasi</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Copyright - End -->

        </div>
    </footer>
    <!-- Footer - End -->