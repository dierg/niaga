    <!-- Secondary navigation - Start -->
    <div class="nav-secondary">
        <div class="container">
            <div class="row mx-n2 justify-content-between">

                <!-- Promotion - Start -->
                <div class="px-2 col d-none d-xl-block">
                    <div class="promotion row mx-n1">
                        <div class="promotion-icon px-1 col-auto">
                            <div class="icon-container">
                                <div class="background fas fa-bookmark"></div>
                                <i class="icon fas fa-tag"></i>
                            </div>
                        </div>
                        <div class="promotion-text px-1 col-auto">
                            <div class="row mx-n1">
                                <div class="px-1 col-auto">
                                    <a href="#">Gratis Ebook 9 Cara Cerdas Menggunakan Domain</a>
                                </div>
                                <div class="px-1 col-auto">
                                    <a href="#">[ x ]</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Promotion - End -->

                <!-- Secondary nav - Start -->
                <div class="px-2 col-auto">
                    <div class="nav-support">

                        <!-- Navigation - Start -->
                        <nav class="nav">
                            <a class="nav-link" href="#">
                                <div class="row mx-n1 h-100">
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <div class="icon-container">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                    </div>
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <span>0274-5305505</span>
                                    </div>
                                </div>
                            </a>
                            <a class="nav-link" href="#">
                                <div class="row mx-n1 h-100">
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <div class="icon-container">
                                            <i class="fas fa-comment-alt"></i>
                                        </div>
                                    </div>
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <span>Live Chat</span>
                                    </div>
                                </div>
                            </a>
                            <a class="nav-link" href="#">
                                <div class="row mx-n1 h-100">
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <div class="icon-container rounded-circle inverted">
                                            <i class="fas fa-user fs-11"></i>
                                        </div>
                                    </div>
                                    <div class="px-1 col-auto d-flex align-items-center">
                                        <span>Member Area</span>
                                    </div>
                                </div>
                            </a>
                        </nav>
                        <!-- Navigation - End -->

                    </div>
                </div>
                <!-- Secondary nav - End -->

            </div>
        </div>
    </div>
    <!-- Secondary navigation - End -->

    <!-- <header class="header"> -->
    <header class="header">

        <!-- Primary navigation - Start -->
        <nav class="nav-primary">
            <div class="container">
                <div class="row mx-n2">
                    <div class="px-2 col-auto">

                        <!-- Main logo - Start -->
                        <router-link to="/" class="logo">
                            <img class="logo-img" src="{{ url('frontend/assets/images/logo.png') }}" alt="Niagahoster Logo">
                        </router-link>
                        <!-- Main logo - End -->

                    </div>
                    <div class="px-2 col d-flex justify-content-end align-items-center">

                        <div class="row mx-n2 d-flex d-xl-none">
                            <div class="px-2 col-auto d-none d-sm-flex align-items-center">

                                <!-- Login button - Start -->
                                <a href="#" class="btn btn-outline-primary fs-14 px-4 rounded-pill">
                                    <span>Login</span>
                                </a>
                                <!-- Login button - End -->

                            </div>
                            <div class="px-2 col-auto">

                                <!-- Support nav button - Start -->
                                <button type="button" class="btn btn-navbar btn-support rounded-circle text-blue">
                                    <i class="lni lni-popup fs-16"></i>
                                </button>
                                <!-- Support nav button - End -->

                            </div>
                            <div class="px-2 col-auto">

                                <!-- Sidebar toggle button - Start -->
                                <button type="button" class="btn btn-navbar btn-slidein rounded-circle text-purple">
                                    <i class="lni lni-menu fs-16"></i>
                                </button>
                                <!-- Sidebar toggle button - End -->

                            </div>
                        </div>
                        

                        <!-- Main naigation - Start -->
                        <div class="nav-main sidebar">

                            <!-- Close button - Start -->
                            <div class="d-flex d-xl-none justify-content-end pt-3 px-4">
                                <button type="button" class="btn btn-xmark btn-slideout"></button>
                            </div>
                            <!-- Close button - End -->

                            <!-- Navigation - Start -->
                            <ul class="nav mt-3 mt-xl-0">

                                <li class="nav-item nav-red">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'hosting' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-globe-asia"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Hosting</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-blue">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'domain' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-trademark"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Domain</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-purple">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'server' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-server"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Server</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-red">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'website' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-desktop"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Website</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-blue">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'affiliate' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-link"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Afiliasi</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-purple">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'promotion' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-percent"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Promo</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-red">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'payment' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-credit-card"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Pembayaran</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-blue">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'reviews' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-comment-dots"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Review</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-purple">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'contact-us' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-phone-alt"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Kontak</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                                <li class="nav-item nav-red">
                                    <router-link class="nav-link" :to="{ name: 'page', params: { page: 'blog' }}">
                                        <div class="row mx-n2">
                                            <div class="px-2 col-auto d-block d-xl-none">
                                                <div class="icon-container icon-container-lg">
                                                    <i class="fas fa-blog"></i>
                                                </div>
                                            </div>
                                            <div class="px-2 col d-flex align-items-center">
                                                <span class="nav-text">Blog</span>
                                            </div>
                                        </div>
                                    </router-link>
                                </li>

                            </ul>
                            <!-- Navigation - End -->
                            
                        </div>
                        <!-- Main naigation - End -->
                    </div>
                </div>
            </div>
        </nav>
        <!-- Primary navigation - End -->

    </header>
    <!-- </header> -->