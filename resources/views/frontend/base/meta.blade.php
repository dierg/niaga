<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Niagahoster</title>

<!-- Components - Start -->
<link rel="stylesheet" href="{{ url('frontend/components/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('frontend/components/@fortawesome/fontawesome-free/css/all.min.css') }}">
<!-- Components - End -->

<!-- CDNs - Start -->
<link rel="stylesheet" href="https://cdn.lineicons.com/2.0/LineIcons.css">
<!-- CDNs - End -->

<!-- Fonts - Start -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;0,900;1,400&family=Roboto:ital,wght@0,300;0,400;0,700;0,900;1,400&display=swap">
<!-- Fonts - End -->

<!-- App stylesheet - Start -->
<link rel="stylesheet" href="{{ url('frontend/assets/css/app.min.css') }}">
<!-- App stylesheet - End -->