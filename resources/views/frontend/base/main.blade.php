<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.base.meta')
</head>

<body>
    <!-- Outset wrapper - Start -->
    <div class="outset" id="app">
        @include('frontend.base.header')

        <main>
            @yield('content')
        </main>
            
        @include('frontend.base.footer')
    </div>
    <!-- Outset wrapper - End -->
            
    <script src="{{ url('frontend/components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ url('frontend/components/stickybits/dist/jquery.stickybits.min.js') }}"></script>
    <script src="{{ url('frontend/assets/js/main.js') }}"></script>

    <script src="{{ url('js/app.js') }}"></script>

</body>
</html>