// --------------------------------------------------------------------------
import Vue from 'vue';
import VueRouter from 'vue-router';
// --------------------------------------------------------------------------
import homePage from './components/homePage';
import planPage from './components/planPage';
import otherPage from './components/otherPage';
// --------------------------------------------------------------------------
Vue.use( VueRouter );
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
// Define routes
// --------------------------------------------------------------------------
export default new VueRouter({
    routes: [
        { path: '/', name: 'home', component: homePage },
        { path: '/plan/:url', name: 'plan', component: planPage },
        { path: '/:page', name: 'page', component: otherPage }
    ],
    mode: 'history'
});
// --------------------------------------------------------------------------