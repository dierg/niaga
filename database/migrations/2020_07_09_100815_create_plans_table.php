<?php
// --------------------------------------------------------------------------
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
class CreatePlansTable extends Migration {
    // ----------------------------------------------------------------------
    public function up() {
        Schema::create( 'plans', function( Blueprint $table ){
            // --------------------------------------------------------------
            $table->bigIncrements('id');
            $table->string('name', 512);
            $table->string('title', 512);
            $table->decimal('price', 19, 2);
            $table->decimal('discount_price', 19, 2)->nullable();
            $table->integer('users' )->unsigned()->nullable();
            $table->string('label', 128)->nullable();
            $table->string('url', 2083)->nullable();
            $table->tinyInteger('featured')->nullable()->default(0);
            $table->tinyInteger('best_seller')->nullable()->default(0);
            // --------------------------------------------------------------
            $table->timestamps();
            // --------------------------------------------------------------
        });
    }
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    public function down(){
        Schema::dropIfExists( 'plans' );
    }
    // ----------------------------------------------------------------------
}
// --------------------------------------------------------------------------