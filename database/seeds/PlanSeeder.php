<?php
// --------------------------------------------------------------------------
use Carbon\Carbon;
use Illuminate\Database\Seeder;
// --------------------------------------------------------------------------
use App\Models\Plan;
use App\Models\PlanDescription as Description;
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
class PlanSeeder extends Seeder {
    public function run(){
        // ------------------------------------------------------------------
        $dataset = array (
            array (
                'name'           => 'bayi',
                'title'          => 'Bayi',
                'price'          => 19900,
                'discount_price' => 14900,
                'users'          => 938,
                'url'            => 'paket-bayi',
                'descriptions' => array(
                    array([ '0.5X RESOURCE POWER' ]),
                    array([ '500 MB', 'Disk Space' ]),
                    array([ 'Unlimited', 'Bandwidth' ]),
                    array([ 'Unlimited', 'Databases' ]),
                    array([ '1', 'Domain' ]),
                    array([ 'Instant', 'Backup' ]),
                    array([ 'Unlimited SSL', 'Gratis Selamanya' ]),
                )
            ),
            array (
                'name'           => 'pelajar',
                'title'          => 'Pelajar',
                'price'          => 46900,
                'discount_price' => 23450,
                'users'          => 4168,
                'url'            => 'paket-pelajar',
                'descriptions' => array(
                    array([ '1X RESOURCE POWER' ]),
                    array([ 'Unlimited', 'Disk Space' ]),
                    array([ 'Unlimited', 'Bandwidth' ]),
                    array([ 'Unlimited', 'POP3 Email' ]),
                    array([ 'Unlimited', 'Databases' ]),
                    array([ '10', 'Addon Domains' ]),
                    array([ 'Instant', 'Backup' ]),
                    array([ 'Domain Gratis', 'Selamanya' ]),
                    array([ 'Unlimited SSL', 'Gratis Selamanya' ]),
                )
            ),
            array (
                'name'           => 'personal',
                'title'          => 'Personal',
                'price'          => 58900,
                'discount_price' => 38900,
                'users'          => 10017,
                'url'            => 'paket-personal',
                'best_seller'    => true,
                'descriptions' => array(
                    array([ '2X RESOURCE POWER' ]),
                    array([ 'Unlimited', 'Disk Space' ]),
                    array([ 'Unlimited', 'Bandwidth' ]),
                    array([ 'Unlimited', 'POP3 Email' ]),
                    array([ 'Unlimited', 'Databases' ]),
                    array([ 'Unlimited', 'Addon Domains' ]),
                    array([ 'Instant', 'Backup' ]),
                    array([ 'Domain Gratis', 'Selamanya' ]),
                    array([ 'Unlimited SSL', 'Gratis Selamanya' ]),
                    array([ 'Private', 'Name Server' ]),
                    array([ 'SpamAssasin', 'Mail Protection' ]),
                )
            ),
            array (
                'name'           => 'bisnis',
                'title'          => 'Bisnis',
                'price'          => 109900,
                'discount_price' => 65900,
                'users'          => 3552,
                'label'          => 'Diskon 40%',
                'url'            => 'paket-bisnis',
                'descriptions' => array(
                    array([ '3X RESOURCE POWER' ]),
                    array([ 'Unlimited', 'Disk Space' ]),
                    array([ 'Unlimited', 'Bandwidth' ]),
                    array([ 'Unlimited', 'POP3 Email' ]),
                    array([ 'Unlimited', 'Databases' ]),
                    array([ 'Unlimited', 'Addon Domains' ]),
                    array([ 'Magic Auto', 'Backup & Restore' ]),
                    array([ 'Domain Gratis', 'Selamanya' ]),
                    array([ 'Unlimited SSL', 'Gratis Selamanya' ]),
                    array([ 'Private', 'Name Server' ]),
                    array([ 'Prioritas', 'Layanan Support' ]),
                    array([ '5' ], 'stars' ),
                    array([ 'SpamExpert', 'Pro Mail Protection' ]),
                    // ------------------------------------------------------
                    // Sample of description with Vue route/page
                    // array([ 'Some', 'Page' ], 'page', 'some-page' )
                    // ------------------------------------------------------
                    // Sample of description with URL
                    // array([ 'Niagahoster', 'Website' ], 'url', 'https://www.niagahoster.co.id' )
                    // ------------------------------------------------------
                )
            ),
        );
        // ------------------------------------------------------------------


        // ------------------------------------------------------------------
        // Loop thru the dataset 
        // ------------------------------------------------------------------
        foreach( $dataset as $dataPlan ){
            // --------------------------------------------------------------
            $plan = new Plan();
            $dataPlan = (object) $dataPlan;
            // --------------------------------------------------------------
            foreach( $dataPlan as $property => $value ){
                if( 'descriptions' !== $property ){
                    $plan->{ $property } = $value;
                }
            } $plan->save(); // Save the plan
            // --------------------------------------------------------------

            // --------------------------------------------------------------
            // Create plan description
            // --------------------------------------------------------------
            if( !empty( $dataPlan->descriptions )){
                $order = 1;
                foreach( $dataPlan->descriptions as $description ){
                    // ------------------------------------------------------
                    $desc = new Description();
                    $desc->plan_id = $plan->id;
                    // ------------------------------------------------------

                    // ------------------------------------------------------
                    // Process description type
                    // ------------------------------------------------------
                    $content = $description[0]; $desc->type = 'text';
                    if( !empty( $description[1])) $desc->type = $description[1];
                    // ------------------------------------------------------

                    // ------------------------------------------------------
                    // Process description content and alt-content
                    // ------------------------------------------------------
                    if( !empty( $content[0])) $desc->content = $content[0];
                    if( !empty( $content[1])) $desc->alt = $content[1];
                    // ------------------------------------------------------

                    // ------------------------------------------------------
                    // Process description with URL
                    // ------------------------------------------------------
                    if( 'url' === $desc->type && !empty( $description[2] )){
                        $desc->url = $description[2];
                    }
                    // ------------------------------------------------------

                    // ------------------------------------------------------
                    // Add the display order
                    // ------------------------------------------------------
                    $desc->order = $order;
                    $order++;
                    // ------------------------------------------------------

                    // ------------------------------------------------------
                    $desc->save(); // Save the plan description
                    // ------------------------------------------------------
                }
            }
            // --------------------------------------------------------------
        }
        // ------------------------------------------------------------------
    }
    // ----------------------------------------------------------------------
}
// --------------------------------------------------------------------------